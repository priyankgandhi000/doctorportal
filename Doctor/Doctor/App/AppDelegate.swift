//
//  AppDelegate.swift
//  Doctor
//
//  Created by Jaydeep on 30/06/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FBSDKLoginKit
import FBSDKCoreKit
import GoogleSignIn
import FirebaseCore
import FirebaseMessaging
import FirebaseInstanceID
import UserNotifications
import SwiftyJSON

protocol CurrentLocationDelegate {
    func didUpdateLocation(lat:Double?,lon:Double?)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK:- Variable Declaration
    
    var delegate:CurrentLocationDelegate?
    var window: UIWindow?
    var locationManager = CLLocationManager()
    var lattitude : Double?
    var longitude : Double?
    
    //MARK:- Appdelegate Method

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        setUpQuickLocationUpdate()
        GMSServices.provideAPIKey(kGoogleMapsKey)
        GMSPlacesClient.provideAPIKey(kGoogleMapsKey)
        Messaging.messaging().delegate = self
        registerForRemoteNotification()
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

//MARK:- Facebbok And Twitter Login

extension AppDelegate
{
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        /*if GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApplication, annotation: annotation) {
            return true
        }*/
        if (GIDSignIn.sharedInstance()?.handle(url))! {
            return true
        }
        
        return  ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation:annotation)
    }
    
    /*func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return ApplicationDelegate.shared.application(app, open: url, sourceApplication: "UIApplicationOpenURLOptionsKey", annotation: nil)
    }*/
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if (GIDSignIn.sharedInstance()?.handle(url))! {
            return true
        }

        return ApplicationDelegate.shared.application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: nil)
    }
}


//MARK:- Location Delegate

extension AppDelegate:CLLocationManagerDelegate
{
    func setUpQuickLocationUpdate()
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
//        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestLocation()
        self.locationManager.requestWhenInUseAuthorization()
        /*self.locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.distanceFilter = 10
        locationManager.activityType = .automotiveNavigation*/
        self.locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        
        if let latestLocation = locations.first
        {
            print("latestLocation:=\(latestLocation.coordinate.latitude), *=\(latestLocation.coordinate.longitude)")
            
            if latestLocation == nil
            {
                setUpQuickLocationUpdate()
                return
            }
           
            lattitude = latestLocation.coordinate.latitude
            longitude = latestLocation.coordinate.longitude
            kUserCurrentLocation = latestLocation.coordinate
            self.delegate?.didUpdateLocation(lat:lattitude,lon:longitude)
            self.locationManager.stopUpdatingLocation()
            
           /* if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude
            {
               
            }*/
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            //    manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            if #available(iOS 10.0, *) {
                openSetting()
                
            } else {
                // Fallback on earlier versions
            }
            break
            //   default:
            // break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error :- \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        locationManager.stopUpdatingLocation()
    }
    
    //MARK:- open Setting
    
    func openSetting()
    {
        let alertController = UIAlertController (title: APP_NAME, message: "Go to setting", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Setting", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        //     let cancelAction = UIAlertAction(title: mapping.string(forKey: "Cancel_key"), style: .default, handler: nil)
        //    alertController.addAction(cancelAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
        
    }
}

//MARK: - Register FCM

extension AppDelegate: MessagingDelegate,UNUserNotificationCenterDelegate {
    
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self           
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
                DispatchQueue.main.async {
                     UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        Defaults.set(token, forKey: "device_token")
        Defaults.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
        print("FCM Token = \(Messaging.messaging().fcmToken ?? "nill")")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
        
    }
    
    // For < 10 This Notification Method is called
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("Push for ios < 10 version")
        print("User Info = ",userInfo)
        
        if application.applicationState == .active {
            
        }
        else if application.applicationState == .background {
            
        }
        
    }
    
    
    // MARK: UNUserNotificationCenter Delegate // >= iOS 10
    // While App on Foreground mode......
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        print("info:=\(JSON(userInfo))")
        
        completionHandler([.alert, .badge, .sound])
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("info:=\(JSON(userInfo))")
        
        if getUserDetail("dc_user_id") == ""{
            return
        }
        dictPushData = JSON(userInfo)
        isComefromPush = true
        let obj = objStoryboard.instantiateViewController(withIdentifier: "LocumVC") as! LocumVC
        navigateUserWithLG(obj: obj)
        completionHandler()
    }
    
    func navigateUserWithLG(obj : UIViewController)
    {
        let navigationController = NavigationController(rootViewController: obj)
        
        let mainViewController = MainViewController()
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 6)
       
        self.window?.rootViewController = mainViewController
        
        UIView.transition(with: self.window!, duration: 0.6, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
    }
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
}

