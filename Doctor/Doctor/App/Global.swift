//
//  Global.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import CoreLocation
import MaterialComponents
import SwiftyJSON


let APP_NAME = "Doctor Portal"
// User current location

var kUserCurrentLocation = CLLocationCoordinate2D()
var kGoogleMapZoomingLevel:Float = 12.0
var kUserCurrency = String()
// Google Maps Key

//let kGoogleMapsKey = "AIzaSyBrFpDuing65cKgaRIO7P8BaxLC1qmwXyM"
let kGoogleMapsKey = "AIzaSyDqKpIfwTWeD4JYSmfTR3oMY2LsId0-eLk"
let kGoogleSignKey = "1032812407629-s7kd6pav6br5el93m5hnu858ecuovjuj.apps.googleusercontent.com"

// Storyboards
let objStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

let appDelegate = UIApplication.shared.delegate as! AppDelegate

//MARK: - Toast
func makeToast(message : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = message
    MDCSnackbarManager.show(messageSnack)
}

var isComefromPush = false
var dictPushData = JSON()
var strCountryDialingCode = String()
//MARK:- Model object

var objUser = User()
var dictFilter = JSON()

//MARK:- Response flag

var strSuccessResponse = "1"
var strDeviceType = "I"


//MARK:-  Loader Property

let Defaults = UserDefaults.standard
var strLoader:String = ""
var LoaderType:Int = 23
var Loadersize = CGSize(width: 30, height: 30)

//MARK:- Get Data from userdefault

func getUserDetail(_ forKey: String) -> String
{
    guard let userDetail = UserDefaults.standard.value(forKey: "userDetail") as? Data else { return "" }
    let data = JSON(userDetail)
    //   print("data \(data)")
    return data[forKey].stringValue
}

func numberFormatter() -> NumberFormatter {
    let removeDecimalFormatter = NumberFormatter()
    removeDecimalFormatter.minimumFractionDigits = 0
    removeDecimalFormatter.maximumFractionDigits = 2
    removeDecimalFormatter.numberStyle = .decimal
    return removeDecimalFormatter
}






