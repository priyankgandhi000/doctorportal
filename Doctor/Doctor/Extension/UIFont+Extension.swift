//
//  UIFont+Extension.swift
//  SEStatus
//
//  Created by Jaydeep on 03/06/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case regular = "Montserrat-Regular"
    case extraBold = "Montserrat-ExtraBold"
    case black = "Montserrat-Black"
    case semibold = "Montserrat-SemiBold"
    case bold = "Montserrat-Bold"
    case light = "Montserrat-Light"
    case ultraLight = "Montserrat-UltraLight"
    case hairline = "Montserrat-Hairline"
    case thin = "Montserrat-Thin"
}

extension UIFont
{
    
}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
