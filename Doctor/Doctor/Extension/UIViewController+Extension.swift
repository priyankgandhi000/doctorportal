//
//  UIViewController+Extension.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import CoreLocation
import DropDown

extension UIViewController:NVActivityIndicatorViewable {
    
    //MARK:- Action Zone
    
    @IBAction func btnBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnFilterBackAction(_ sender:UIButton){
        self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- SetupNavigation bar
    
    func setUpNavigationBarWithTwoMenu(strTitle:String)
    {
        setupNavigationbar()
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_sidemenu"), style: .plain, target: self, action: #selector(openLeftViewController))
        leftButton.tintColor = UIColor.appThemeBlue
        self.navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_add_locum"), style: .plain, target: self, action: #selector(btnAddLocumAction))
        rightButton.tintColor = UIColor.appThemeBlue
        self.navigationItem.rightBarButtonItem = rightButton
        
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBlack
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 20, fontname: .light)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithTwoRightMenu(strTitle:String)
    {
        setupNavigationbar()
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_black"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBlack
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 20, fontname: .light)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithPresent(strTitle:String)
    {
        setupNavigationbar()
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close"), style: .plain, target: self, action: #selector(btnFilterBackAction(_:)))
        leftButton.tintColor = UIColor.appThemeBlue
        self.navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(image: nil, style: .plain, target: self, action: #selector(btnClearFilterAction(_:)))
        rightButton.title = "Clear"
        rightButton.tintColor = UIColor.appThemeBlue
        self.navigationItem.rightBarButtonItem = rightButton
        
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBlack
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 20, fontname: .light)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithBackButton(strTitle:String)
    {
        setupNavigationbar()
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_black"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
        leftButton.tintColor = UIColor.black
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBlack
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 20, fontname: .light)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    func setUpNavigationBarWithSideMenu(strTitle:String)
    {
        setupNavigationbar()
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_sidemenu"), style: .plain, target: self, action: #selector(openLeftViewController))
        leftButton.tintColor = UIColor.appThemeBlue
        self.navigationItem.leftBarButtonItem = leftButton
        
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.appThemeBlack
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = themeFont(size: 20, fontname: .light)
        
        self.navigationItem.titleView = HeaderLabel
    }
    
    @objc func openLeftViewController() {
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FadeOutEffect"), object: nil)
        sideMenuController?.leftViewController?.showLeftViewAnimated(true)
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FadeInEffect"), object: nil)
    }
    
    @objc func btnAddLocumAction() {
        
    }    
   
    
    @objc func btnClearFilterAction(_ sender:UIButton){
        
    }
    
    func navigateUserWithLG(obj : UIViewController)
    {
        let navigationController = NavigationController(rootViewController: obj)
        
        let mainViewController = MainViewController()
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 6)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.6, options: [.transitionCrossDissolve], animations: nil, completion: nil)

    }
    
    func setupNavigationbar(){
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.clear.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.4
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    func getCurrencyCode() -> String{
        let locale = Locale.current
        let currencySymbol = locale.currencySymbol!
        return currencySymbol
    }
    
    //MARK: - Configure Dropdown
    func configureDropdown(dropdown : DropDown,sender:UIBarButtonItem)
    {
        dropdown.clearSelection()
        dropdown.anchorView = sender
        dropdown.direction = .any
        dropdown.dismissMode = .automatic
//        dropdown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        //        dropdown.topOffset = CGPoint(x: 0, y: sender.bounds.height)
        dropdown.width = 150
        dropdown.cellHeight = 40.0
        dropdown.backgroundColor = UIColor.white
        dropdown.cancelAction = { [unowned self] in
            print("Drop down dismissed")
        }
    }
    
    func alertActionForOpenMaps(lat:Double,long:Double)  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: APP_NAME, message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let googleAction = UIAlertAction(title: "Google Map", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if lat != 0.0 && long != 0.0 {
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {  //if phone has an app
                    
                    if let url = URL(string: "comgooglemaps-x-callback://?saddr=&daddr=\(lat),\(long)&directionsmode=driving") {
                        UIApplication.shared.open(url, options: [:])
                    }}
                else {
                    //Open in browser
                    if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(lat),\(long)&directionsmode=driving") {
                        UIApplication.shared.open(urlDestination)
                    }
                }
            }
            
        }
        
        
        let wazeAction = UIAlertAction(title: "Waze", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if lat != 0.0 && long != 0.0 {
                if let url = URL(string: "waze://") {
                    if UIApplication.shared.canOpenURL(
                        url) {
                        // Waze is installed. Launch Waze and start navigation
                        let urlStr = "https://waze.com/ul?ll=\(lat),\(long)&navigate=yes"
                        if let url = URL(string: urlStr) {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    } else {
                        // Waze is not installed. Launch AppStore to install Waze app
                        /*if let url = URL(
                         string: "http://itunes.apple.com/us/app/id323229106") {
                         UIApplication.shared.open(url, options: [:], completionHandler: nil)
                         }*/
                        
                        if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(lat),\(long)&directionsmode=driving") {
                            UIApplication.shared.open(urlDestination)
                        }
                    }
                }
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(googleAction)
        alertController.addAction(wazeAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }

    //MARK: - StartAnimating
    
    func showLoader()
    {
        startAnimating(Loadersize, message:strLoader , type: NVActivityIndicatorType.ballSpinFadeLoader)
    }
    
    func stopLoader()
    {
        self.stopAnimating()
    }
    
    func getAddressFromLatLon(isGetCurrency:Bool = false,latitude: Double, longitude: Double,completion:@escaping(String)-> ()) {
        
        let ceo: CLGeocoder = CLGeocoder()
        
        let loc: CLLocation = CLLocation(latitude:latitude, longitude: longitude)
        
        var addressString : String = ""
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if let pm = placemarks {
                    if pm.count > 0 {
                        
                        let pm = placemarks![0]
                        print("country",pm.country ?? "")
                        print("locality",pm.locality ?? "")
                        print("subLocality",pm.subLocality ?? "")
                        print("thoroughfare",pm.thoroughfare ?? "")
                        print("postalCode",pm.postalCode ?? "")
                        print("subThoroughfare",pm.subThoroughfare ?? "")
                        print("isoCountryCode",pm.isoCountryCode ?? "")
                        if pm.isoCountryCode != "" {
                            strCountryDialingCode = self.getCountryCallingCode(countryRegionCode: pm.isoCountryCode!)
                        }
                        if isGetCurrency == true {
                            kUserCurrency = self.getCurrency(countryName: pm.country ?? "")
                        }
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        
                        print("addressString \(addressString)")
                        completion(addressString)
                    }
                }
                completion(addressString)
                
        })
        
    }
    
    func getCountryCallingCode(countryRegionCode:String)->String{
        
        let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
        let countryDialingCode = prefixCodes[countryRegionCode]
        return countryDialingCode!
    }
    
    func getSymbolForCurrencyCode(code: String) -> String {
        var candidates: [String] = []
        let locales: [String] = NSLocale.availableLocaleIdentifiers
        for localeID in locales {
            guard let symbol = findMatchingSymbol(localeID: localeID, currencyCode: code) else {
                continue
            }
            if symbol.count == 1 {
                return symbol
            }
            candidates.append(symbol)
        }
        let sorted = sortAscByLength(list: candidates)
        if sorted.count < 1 {
            return ""
        }
        return sorted[0]
    }
    
    func findMatchingSymbol(localeID: String, currencyCode: String) -> String? {
        let locale = Locale(identifier: localeID as String)
        guard let code = locale.currencyCode else {
            return nil
        }
        if code != currencyCode {
            return nil
        }
        guard let symbol = locale.currencySymbol else {
            return nil
        }
        return symbol
    }
    
    func sortAscByLength(list: [String]) -> [String] {
        return list.sorted(by: { $0.count < $1.count })
    }
    
    func getCurrency(countryName:String) -> String{
        if countryName == ""{
            return ""
        }
        let countryCode = Locale.locales(strCountryName: countryName)
        let countryCodeCA = countryCode
        let localeIdCA = NSLocale.localeIdentifier(fromComponents: [ NSLocale.Key.countryCode.rawValue : countryCodeCA])
        let localeCA = NSLocale(localeIdentifier: localeIdCA)
        if let currencyCodeCA = localeCA.object(forKey: NSLocale.Key.currencyCode) as? String{
            return getSymbolForCurrencyCode(code: currencyCodeCA)
        }
        return getSymbolForCurrencyCode(code: "India")
    }
    
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem:  UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = UIColor(red: 35/255, green: 141/255, blue: 250/255, alpha: 1.0)
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        textfield.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
    //MARK: - Date
    func StringToDate(Formatter : String,strDate : String,timeZone:TimeZone =  NSTimeZone.local) -> Date
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        dateformatter.timeZone = timeZone
        
        guard let convertedDate = dateformatter.date(from: strDate) else {
            let str = dateformatter.string(from: Date())
            return dateformatter.date(from: str)!
            
        }
//        print("convertedDate - ",convertedDate)
        return convertedDate
    }
    func DateToString(Formatter : String,date : Date,timeZone:TimeZone =  NSTimeZone.local) -> String
    {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = Formatter
        dateformatter.timeZone = timeZone
        //    dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let convertedString = dateformatter.string(from: date)
        return convertedString
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
   
}
