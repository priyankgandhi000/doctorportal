//
//  LocumDetailCell.swift
//  Doctor
//
//  Created by Jaydeep on 04/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import HCSStarRatingView
import SwiftyJSON

class LocumDetailCell: UITableViewCell {
    
    //MARK:- Oulet Zone
    
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblDoctorExperience: UILabel!
    @IBOutlet weak var lblCompleted: UILabel!
    
    @IBOutlet weak var lblDisplayDoctor: UILabel!
    @IBOutlet weak var btnHireOutlet: CustomButton!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    //MARK:- Viewlife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupData(dict:JSON){
        
    }

}
