//
//  RateCell.swift
//  Doctor
//
//  Created by Jaydeep on 01/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import HCSStarRatingView

class RateCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
     @IBOutlet weak var lblRatingName: UILabel!
     @IBOutlet weak var vwRate: HCSStarRatingView!
    
    //MARK:- TablvieeLifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
