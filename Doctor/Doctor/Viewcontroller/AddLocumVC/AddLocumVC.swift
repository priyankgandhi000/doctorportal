
//
//  AddLocumVC.swift
//  Doctor
//
//  Created by Jaydeep on 03/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import GooglePlaces
import CoreLocation

class AddLocumVC: UIViewController {
    
    //MARK:- Variable Declration
    
    var clinicLatLong = CLLocationCoordinate2D()
    var handlerAddLocum:() -> Void = {}
    var dictLocumDetail = JSON()
    var selectedController = checkAddEditLocum.add
    //MARK:- Outlet Zone
    
    @IBOutlet weak var txtClinicName: CustomTextField!
    @IBOutlet weak var txtLocation: UITextView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var lblDescriptionOutlet: UILabel!
    @IBOutlet weak var txtvwDescription: UITextView!
    @IBOutlet weak var txtRate: CustomTextField!
    @IBOutlet weak var txtHour: CustomTextField!
    @IBOutlet weak var txtDate: CustomTextField!
    @IBOutlet weak var txtTime: CustomTextField!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var btnAddOutlet: CustomButton!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()       
    }

    override func viewWillAppear(_ animated: Bool) {
        if selectedController == .add {
            setUpNavigationBarWithBackButton(strTitle: "Add Locum")
        } else {
            setUpNavigationBarWithBackButton(strTitle: "Edit Locum")
            btnAddOutlet.setTitle("Edit", for: .normal)
        }
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
        self.txtvwDescription.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        self.txtLocation.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        clinicLatLong = kUserCurrentLocation
        
        appDelegate.delegate = self
        
        [txtDate,txtTime,txtClinicName].forEach { (txtField) in
            txtField?.delegate = self
        }
        
        addDoneButtonOnKeyboard(textfield: txtHour)
        addDoneButtonOnKeyboard(textfield: txtRate)
        
        
        
        if selectedController == .edit {
            self.txtClinicName.text = dictLocumDetail["firm_name"].stringValue
            self.txtLocation.text = dictLocumDetail["location"].stringValue
            
            let strPrice = numberFormatter().string(from: NSNumber(value: dictLocumDetail["rate_hr"].floatValue)) ?? ""
            
            self.txtRate.text = "\(strPrice)"
            self.txtHour.text = dictLocumDetail["total_hr"].stringValue
            
            let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dictLocumDetail["date_time_from"].stringValue)
            let strFromDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: startDate)
            let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
            
            self.txtDate.text = strFromDate
            self.txtTime.text = strFromTime
            self.txtvwDescription.text = dictLocumDetail["comments"].stringValue
            
            self.lblPlaceholder.isHidden = dictLocumDetail["location"].stringValue.isEmpty ? false : true
            self.lblDescriptionOutlet.isHidden = dictLocumDetail["comments"].stringValue.isEmpty ? false : true
            
            lblCurrency.text = dictLocumDetail["currency"].stringValue
            
            clinicLatLong = CLLocationCoordinate2D(latitude: dictLocumDetail["lat"].doubleValue, longitude: dictLocumDetail["lng"].doubleValue)
        } else {
            if getUserDetail("type").lowercased() != checkDoctorClinic.doctor.rawValue.lowercased()  {
                self.txtClinicName.text = getUserDetail("clinic_name")
                clinicLatLong = CLLocationCoordinate2D(latitude: Double(getUserDetail("lat")) ?? 0.0, longitude: Double(getUserDetail("lng")) ?? 0.0)
                self.txtLocation.text = getUserDetail("address")
                self.lblPlaceholder.isHidden = getUserDetail("address").isEmpty ? false : true
            }
            
            getAddressFromLatLon(isGetCurrency: true,latitude: kUserCurrentLocation.latitude, longitude: kUserCurrentLocation.longitude) { (currentAddress) in
                self.lblCurrency.text = kUserCurrency
            }

        }
    }
    
    //MARK:- Action Zone
    
    @IBAction func btnAddLocumDetailAction() {
        objUser = User()
        objUser.strClinicName = txtClinicName.text ?? ""
        objUser.strClinicAddress = txtLocation.text ?? ""
        objUser.strLocumRate = txtRate.text ?? ""
        objUser.strLocumTotalHr = txtHour.text ?? ""
        objUser.strLocumStartDate = txtDate.text ?? ""
        objUser.strLocumStartTime = txtTime.text ?? ""
    
        if objUser.isAddNewLocum() {
            let fullDate = "\(self.txtDate.text ?? "") \(self.txtTime.text ?? "")"
            
            let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: fullDate)
            let calendar = Calendar.current
            let endDate = calendar.date(byAdding: .hour, value: 2, to: Date())
            
            if finalDate < Date() {
                self.txtTime.text = ""
                makeToast(message: "Oops! sorry can't post too close to the current time. You can only post a new locum 2 hours from the current time.")
                return
            }
//            let strTmpTime = DateToString(Formatter: dateFormatter.dateFormate3.rawValue, date: self.txtTime.text)
            
            /*let fullDate = "\(self.txtDate.text ?? "") \(self.txtTime.text ?? "")"
            
            let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: fullDate)
            let calendar = Calendar.current
            let endDate = calendar.date(byAdding: .hour, value: 2, to: Date())
            
            if finalDate < Date() {
                self.txtTime.text = ""
                makeToast(message: "Oops! sorry can't post too close to the current time. You can only post a new locum 2 hours from the current time.")
                return
            }*/
            addLocum()
        } else {
            makeToast(message: objUser.strValidationMessage)
        }
    }
    
    
    @IBAction func btnCurrentLocation(_ sender:UIButton){
        appDelegate.setUpQuickLocationUpdate()
    }

}

//MARK:- get Current Location Delegate

extension AddLocumVC:CurrentLocationDelegate {
    func didUpdateLocation(lat: Double?, lon: Double?) {
        
        if lat == nil && lon == nil {
            return
        }
        clinicLatLong = CLLocationCoordinate2D(latitude: lat ?? 20.5937, longitude: lon ?? 78.9629)
        //        showLoader()
        let strLocation = getAddressFromLatLon(latitude: clinicLatLong.latitude, longitude: clinicLatLong.longitude) { (currentAddress) in
            self.stopLoader()
            self.txtLocation.text = currentAddress
            self.lblPlaceholder.isHidden = self.txtLocation.text.isEmpty ? false : true
        }
    }
}

//MARK:- UITextview Delegate

extension AddLocumVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView == txtvwDescription {
            if textView.text == "" {
                self.lblDescriptionOutlet.isHidden = false
            } else {
                self.lblDescriptionOutlet.isHidden = true
            }
        } else {
            if textView.text == "" {
                self.lblPlaceholder.isHidden = false
            } else {
                self.lblPlaceholder.isHidden = true
            }
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        if textView == txtLocation {
            setupGooglePlacePicker()
            return false
        }
        return true        
    }
}

//MARK:- UITextfield Delegate
extension AddLocumVC:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDate {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.type = 0
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = true
            obj.isSetDate = true
            
            obj.setDateValue = Date()
            obj.minimumDate = Date()
            
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.date)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        } else if textField == txtTime {
            if txtDate.text == "" {
                makeToast(message: "Please select date first")
                return false
            }
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.isSetMaximumDate = false
            obj.isSetMinimumDate = false
            obj.isSetDate = false
            obj.type = 1
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePicker.Mode.time)
            
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.becomeFirstResponder()
    }
}

//MARK:- Picker Delegate

extension AddLocumVC : datePickerDelegate
{
    func setDateValue(dateValue: Date,type:Int) {
        if type == 0
        {
            let strDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: dateValue)
            self.txtDate.text = strDate
        }
        else
        {
            let strTmpTime = DateToString(Formatter: dateFormatter.dateFormate3.rawValue, date: dateValue)
            
            let fullDate = "\(self.txtDate.text ?? "") \(strTmpTime)"
            
            let finalDate = StringToDate(Formatter: dateFormatter.dateFormate8.rawValue, strDate: fullDate)
            let calendar = Calendar.current
            let endDate = calendar.date(byAdding: .hour, value: 2, to: Date())
            
            if finalDate < Date() {
                self.txtTime.text = ""
                makeToast(message: "Oops! sorry can't post before current time")
                return
            }
            
            let strTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: dateValue)
            self.txtTime.text = strTime
        }
    }
}


//MARK:- Google Place Picker

extension AddLocumVC:GMSAutocompleteViewControllerDelegate {
    
    func setupGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.all.rawValue))!
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        /*let filter = GMSAutocompleteFilter()
         filter.type = .city
         autocompleteController.autocompleteFilter = filter*/
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place ID: \(place.placeID)")
        print("Place attributions: \(place.attributions)")
        print("Place coordinate: \(place.coordinate)")
        print("Place formattedAddress: \(place.formattedAddress)")
        
        clinicLatLong = place.coordinate
        
        self.txtLocation.text = place.formattedAddress ?? ""
        self.lblPlaceholder.isHidden = self.txtLocation.text == "" ? false : true
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


//MARK:- Service


extension AddLocumVC
{
    func addLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let strStartDate = "\(self.txtDate.text!) \(self.txtTime.text!)"
            
            let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: strStartDate)
            
            let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: finalDate)
            
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .hour, value: Int(txtHour.text ?? "0") ?? 0, to: finalDate)
            
            let strFinalToDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: date ?? Date())
            
            var url = String()
            
            if selectedController == .add {
                url = "\(kBasicURL)\(kAddLocum)"
            } else {
                url = "\(kBasicURL)\(kEditLocum)"
            }
            
            print("URL: \(url)")
            
            var param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "firm_name":txtClinicName.text ?? "",
                          "location":txtLocation.text ?? "",
                          "rate_hr":txtRate.text ?? "",
                          "total_hr":txtHour.text ?? "",
                          "date_time_from":strFinalFromDate,
                          "date_time_to":strFinalToDate,
                          "lat":"\(clinicLatLong.latitude)",
                          "lng":"\(clinicLatLong.longitude)",
                          "comments":txtvwDescription.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                          "currency":self.lblCurrency.text ?? getCurrencyCode()]
            
            
            if selectedController == .edit {
                param["dc_locum_id"] = dictLocumDetail["dc_locum_id"].stringValue
            }
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["message"].stringValue)
                        self.handlerAddLocum()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}

