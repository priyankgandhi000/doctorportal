//
//  CalendarVC.swift
//  Doctor
//
//  Created by Jaydeep on 06/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import FSCalendar
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class CalendarVC: UIViewController {
    
    //MARK:- Variablde Declaration'
    
    var arrLocumList:[JSON] = []
    var handlerGetAllLocum:() -> Void = {}
    var strMessage = String()
    var currentMonth =  String()
    var currentYear =  String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblMonthName: UILabel!
    @IBOutlet weak var vwCalendar: FSCalendar!
    @IBOutlet weak var tblLocumDetail: UITableView!
    @IBOutlet weak var heightOfTblLocumDetail: NSLayoutConstraint!
    @IBOutlet weak var scrollbarMain: UIScrollView!
    
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithSideMenu(strTitle: "Calendar")
        tblLocumDetail.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblLocumDetail.removeObserver(self, forKeyPath: "contentSize")
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblLocumDetail.contentSize.height)")          
            self.heightOfTblLocumDetail.constant = tblLocumDetail.contentSize.height
        }
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
        self.scrollbarMain.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        vwCalendar.delegate = self
        vwCalendar.dataSource  = self
        vwCalendar.placeholderType = .none
        GetSelectedMonthDataService(date: Date())
    }
}

//MARK:- Private MEthod

extension CalendarVC : FSCalendarDataSource,FSCalendarDelegate,FSCalendarDelegateAppearance {
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        GetSelectedMonthDataService(date: calendar.currentPage)
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        for i in 0..<arrLocumList.count{
            let dict = arrLocumList[i]
            let dtFromTime = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
            let strFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: dtFromTime)
            let dtFrom = StringToDate(Formatter: dateFormatter.dateFormate6.rawValue, strDate: strFromDate)
            
            let strDtMain = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: date)
            let dtFromMain = StringToDate(Formatter: dateFormatter.dateFormate6.rawValue, strDate: strDtMain)
            
            if dtFrom == dtFromMain {
                if dict["dc_user_id"].stringValue == getUserDetail("dc_user_id"){
                    return UIColor.white
                } else {
                    return UIColor.white
                }
            }
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        for i in 0..<arrLocumList.count{
            let dict = arrLocumList[i]
            let dtFromTime = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
            let strFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: dtFromTime)
            let dtFrom = StringToDate(Formatter: dateFormatter.dateFormate6.rawValue, strDate: strFromDate)
            
            let strDtMain = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: date)
            let dtFromMain = StringToDate(Formatter: dateFormatter.dateFormate6.rawValue, strDate: strDtMain)
            
            if dtFrom == dtFromMain {               
                /*print("dict \(dict)")
                print("calDate \(dtFromMain)")
                print("dtFromSelected \(dtFrom)")*/
                if dict["dc_user_id"].stringValue == getUserDetail("dc_user_id"){
                    /*if dict["repeat"].intValue > 1{
                        return hexStringToUIColor(hex: "#BB8283")
                    }*/
                    return UIColor.black
                } else {
                    /*if dict["repeat"].intValue > 1{
                        return hexStringToUIColor(hex: "#BB8283")
                    }*/
                    return hexStringToUIColor(hex: "#238DFA")
                }
            }
        }
        return nil
    }
    func previousMonth()
    {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = -1 // For prev button
        let currentPg = vwCalendar.currentPage
        let currentPage = _calendar.date(byAdding: dateComponents, to: currentPg)
        GetSelectedMonthDataService(date:currentPage ?? Date())
    }
    
    func nextMonth()
    {
        let _calendar = Calendar.current
        var dateComponents = DateComponents()
        dateComponents.month = 1 // For next button
        let currentPg = vwCalendar.currentPage
        let currentPage = _calendar.date(byAdding: dateComponents, to:currentPg)
        GetSelectedMonthDataService(date:currentPage ?? Date())
    }
    
    func GetSelectedMonthDataService(date:Date)
    {
        self.lblMonthName.text = "\(date.month) \(date.year)"
        self.vwCalendar.setCurrentPage(date, animated: true)
        currentMonth = date.monthDigit
        currentYear = date.year
        getCalendarList(month: "\(date.monthDigit)", year: "\(date.year)")
    }
}

//MARK:- Action Zone

extension CalendarVC {
    
    @IBAction func btnNextMonthAction(_ sender:UIButton){
         nextMonth()
    }
    
    @IBAction func btnPreviousMonthAction(_ sender:UIButton){
        previousMonth()
    }
    
    @IBAction func btnSortAction(_ sender:UIButton){
        arrLocumList = arrLocumList.reversed()
        self.tblLocumDetail.reloadData()        
    }
    
    @IBAction func btnOpenMapsAction(_ sender:UIButton) {
        let dict = arrLocumList[sender.tag]
        let lat = dict["lat"].doubleValue
        let long = dict["lng"].doubleValue
        
        alertActionForOpenMaps(lat: lat, long: long)
    }
    
}

//MARK:- Tablview Delegate and Datasource

extension CalendarVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrLocumList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrLocumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocumCell") as! LocumCell
        let dict = arrLocumList[indexPath.row]
        cell.lblHospitalName.text = dict["firm_name"].stringValue
        cell.lblHospitalAddress.text = dict["location"].stringValue
        
        let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
        let strFromDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: startDate)
        let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
        cell.lblFromDate.text = "\(strFromDate) | \(strFromTime)"
        
        let toDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_to"].stringValue)
        let strToDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: toDate)
        let strToTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: toDate)
        
        cell.lblToDate.text = "\(strToDate) | \(strToTime)"
        cell.vwRating.value = CGFloat(dict["rate"].floatValue)
        cell.vwAvgRate.value = CGFloat(dict["avg_rate"].floatValue)
        
        let price = dict["rate_hr"].floatValue * dict["total_hr"].floatValue
        
        let strFinalCurreny = numberFormatter().string(from: NSNumber(value: dict["rate_hr"].floatValue)) ?? ""
        
        let strPrice = numberFormatter().string(from: NSNumber(value: price)) ?? ""
        
        cell.lblRate.text = "Rate: \(dict["currency"].stringValue)\(strFinalCurreny)/hr (\(dict["currency"].stringValue)\(strPrice) for \(dict["total_hr"].stringValue)hr)"
        
        cell.vwStatus.isHidden = true
        
        switch dict["is_cal_status"].stringValue {
        case "2":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Selected"
            } else {
                cell.lblStatus.text = "Hired"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#4CAF50")
            break
        case "3":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Waiting"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#FFC107")
            break
        case "4":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Rejected"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "5":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Completed"
            } else {
                cell.lblStatus.text = "Hired Completed"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#238DFA")
            break
        case "6":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Expired"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "7":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Cancelled"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        default:
            cell.vwStatus.isHidden = true
        }
        
        if dict["dc_user_id"].stringValue == getUserDetail("dc_user_id"){
            cell.vwIndicator.backgroundColor = UIColor.black
        } else {
            cell.vwIndicator.backgroundColor = hexStringToUIColor(hex: "#238DFA")
        }
        
        cell.btnRedirectionOutlet.tag = indexPath.row
        cell.btnRedirectionOutlet.addTarget(self, action: #selector(btnOpenMapsAction), for: .touchUpInside)
        
//        print("dict Tableview \(dict)")
        cell.contentView.layoutIfNeeded()
        cell.contentView.layoutSubviews()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = arrLocumList[indexPath.row]
        dict["is_final_status"].stringValue = dict["is_cal_status"].stringValue
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumDetailVC") as! LocumDetailVC
        obj.handlerRefreshLocum = {[weak self] in
            self?.getCalendarList(month: self?.currentMonth ?? "1", year: self?.currentYear ?? "2019")
        }
        obj.dictLocumDetail = dict
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Service

extension CalendarVC
{
    func getCalendarList(month:String,year:String)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kCalendarList)"
            
            print("URL: \(url)")
            
            let param = ["dc_user_id":getUserDetail("dc_user_id"),
                         "year":"\(year)",
                         "month":"\(month)"]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.arrLocumList = []
                        self.arrLocumList = json["data"].arrayValue
                        if self.arrLocumList.count == 0{
                            makeToast(message: json["message"].stringValue)
                        }
                        
                        for i in 0..<self.arrLocumList.count{
                            var dict = self.arrLocumList[i]
                            let dtFromTime = self.StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
                            let strFromDate = self.DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: dtFromTime)
                            dict["date_convert"].stringValue = strFromDate
                            print("Date \(dict["date_time_from"].stringValue)")
                            self.arrLocumList[i] = dict
                        }
                        let strArr = self.arrLocumList.map({$0["date_convert"].stringValue})
                        
                        var counts: [String: Int] = [:]
                        
                        for item in strArr {
                            counts[item] = (counts[item] ?? 0) + 1
                        }
                        
                        for (key, value) in counts {
                            print("\(key) occurs \(value) time(s)")
                            for i in 0..<self.arrLocumList.count{
                                var dict = self.arrLocumList[i]
                                if dict["date_convert"].stringValue == key{
                                    dict["repeat"].intValue = value
                                    self.arrLocumList[i] = dict
                                }
                            }
                        }
                    }
                    else
                    {
                        self.arrLocumList = []
                        self.strMessage = json["message"].stringValue
                        makeToast(message: json["message"].stringValue)
                    }
                    self.vwCalendar.reloadData()
                    self.tblLocumDetail.reloadData()
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}

