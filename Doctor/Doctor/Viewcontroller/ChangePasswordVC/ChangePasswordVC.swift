//
//  ChangePasswordVC.swift
//  Doctor
//
//  Created by Jaydeep on 03/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ChangePasswordVC: UIViewController {
    
    //MARK:- Variable Zone
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    //MARK:-  Outlet Zone
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithBackButton(strTitle: "Change Password")
    }

}

//MARK:- Action ZOne

extension ChangePasswordVC {
    
    @IBAction func btnChangePasswordAction(_ sender:UIButton){
        objUser = User()
        objUser.strOldPassword = txtOldPassword.text ?? ""
        objUser.strNewPassword = txtNewPassword.text ?? ""
        objUser.strConfirmPassword = txtConfirmPassword.text ?? ""
        
        if objUser.isChangePassword()
        {
            isChangePassword()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
    }
}

//MARK:- Service

extension ChangePasswordVC
{
    func isChangePassword()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            let url = "\(kBasicURL)\(kChangePassword)"
            
            print("URL: \(url)")
            
            let param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "password":self.txtNewPassword.text ?? "",
                          "old_password":self.txtOldPassword.text ?? ""
            ]
            
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.navigationController?.popViewController(animated: true)
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
}
