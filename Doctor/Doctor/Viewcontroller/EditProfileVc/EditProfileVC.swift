//
//  EditProfileVC.swift
//  Doctor
//
//  Created by Jaydeep on 03/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage
import GooglePlaces
import CoreLocation

class EditProfileVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictUserDetail = JSON()
    let picker = UIImagePickerController()
    var clinicLatLong = CLLocationCoordinate2D()
    var handlerEdirProfile:() -> Void = {}
    var arrYear:[JSON] = []
    var isSelectedImage = Bool()
    var strStartingYear = String()
    
    //MARK:- Outlet Zone
    @IBOutlet weak var txtVwLocation: UITextView!
    @IBOutlet weak var lblLocationPlaceholder: UILabel!
    @IBOutlet weak var txtVwDescription: UITextView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgProfile: CustomImageView!
    @IBOutlet weak var txtUserFullName: UITextField!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtMobileNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var vwDoctor: UIView!
    @IBOutlet weak var txtDoctorRegistration: UITextField!
    @IBOutlet weak var vwClinicName: UIView!
    @IBOutlet weak var txtClinicName: UITextField!
    @IBOutlet weak var vwClinicAddress: UIView!
    @IBOutlet weak var vwDescription: UIView!
    @IBOutlet weak var txtCountryCode: UITextField!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithBackButton(strTitle: "Edit Profile")
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
        
        if dictUserDetail["type"].stringValue == checkDoctorClinic.doctor.rawValue{
            [vwClinicName,vwClinicAddress].forEach { (view) in
                view?.isHidden = true
            }
            
            vwDoctor.isHidden = false
        } else {
            [vwClinicName,vwClinicAddress].forEach { (view) in
                view?.isHidden = false
            }
            
            vwDoctor.isHidden = true
        }
        [txtVwLocation,txtVwDescription].forEach { (txtView) in
            txtView?.delegate = self
        }
        
        [txtUserFullName,txtCountryCode,txtMobileNumber,txtEmail,txtDoctorRegistration,txtClinicName].forEach { (txtField) in
            txtField?.delegate = self
        }
        
        imgProfile.sd_setImage(with: dictUserDetail["profile_url"].url , placeholderImage:nil, options: .lowPriority, completed: nil)
        imgProfile.cornerRadius = imgProfile.frame.height / 2
        imgProfile.clipsToBounds = true
        
        if let img = imgProfile.image {
            isSelectedImage = true
        }
        
        txtUserFullName.text = dictUserDetail["user_name"].stringValue
        txtEmail.text = dictUserDetail["email_id"].stringValue
        txtMobileNumber.text = "\(dictUserDetail["contact_number"].stringValue)"
        txtCountryCode.text = dictUserDetail["country_code"].stringValue
        txtClinicName.text = dictUserDetail["clinic_name"].stringValue
        txtDoctorRegistration.text = "Starting year :- \(dictUserDetail["year_experience"].stringValue)"
        txtVwDescription.text = dictUserDetail["bio"].stringValue
        lblDescription.isHidden = txtVwDescription.text.isEmpty ? false : true
        txtVwLocation.text = dictUserDetail["address"].stringValue
        lblLocationPlaceholder.isHidden = txtVwLocation.text.isEmpty ? false : true
        
        clinicLatLong = CLLocationCoordinate2D(latitude: dictUserDetail["lat"].doubleValue, longitude: dictUserDetail["lng"].doubleValue)
        
        strStartingYear = dictUserDetail["year_experience"].stringValue
        
        setupYearArray()
    }
    
    func setupYearArray()
    {
        arrYear = []
        let date = Date()
        let calendar = Calendar.current
        let year = calendar.component(.year, from: date)
        print("year \(year)")
        
        for i in stride(from: year-50, to: year+1, by: 1) {
            var dict = JSON()
            dict["value"] = JSON(String(i))
            arrYear.append(dict)
        }
        print("arrYear \(arrYear)")
    }
}

//MARK:- UITextfield Delegate

extension EditProfileVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == txtCountryCode {
            let maxLength = 4
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        } else if textField == txtMobileNumber {
            let maxLength = 10
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtDoctorRegistration {
            self.view.endEditing(true)
            let obj = CommonPickerVC()
            obj.pickerDelegate = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.arrayPicker = arrYear
            self.present(obj, animated: false, completion: nil)
            return false
        }
        return true
    }
}

extension EditProfileVC :CommonPickerDelegate
{
    func setValuePicker(selectedDict: JSON) {
        strStartingYear = selectedDict["value"].stringValue
        self.txtDoctorRegistration.text = "Starting year :- \(selectedDict["value"].stringValue)"
    }
    
}

//MARK:- Textview Delegate

extension EditProfileVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView == txtVwDescription {
            if textView.text == "" {
                self.lblDescription.isHidden = false
            } else {
                self.lblDescription.isHidden = true
            }
        } else {
            if textView.text == "" {
                self.lblLocationPlaceholder.isHidden = false
            } else {
                self.lblLocationPlaceholder.isHidden = true
            }
        }
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.view.endEditing(true)
        if textView == txtVwLocation {
            setupGooglePlacePicker()
            return false
        }
        return true
    }
}

//MARK:- Google Place Picker

extension EditProfileVC:GMSAutocompleteViewControllerDelegate {
    
    func setupGooglePlacePicker(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
       
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
            UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue) | UInt(GMSPlaceField.all.rawValue))!
        autocompleteController.placeFields = fields
     
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        clinicLatLong = place.coordinate
        
        self.txtVwLocation.text = place.formattedAddress ?? ""
        self.lblLocationPlaceholder.isHidden = self.txtVwLocation.text == "" ? false : true
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}


//MARK:- Action Zone

extension EditProfileVC {
    
    @IBAction func btnSelectProfileAction(_ sender:UIButton){
        alertActionForCamara()
    }
    
    @IBAction func btnChangeProfileAction(_ sender:UIButton){
        objUser = User()
        objUser.isImage = isSelectedImage
        objUser.strFullName = self.txtUserFullName.text ?? ""
        objUser.strCountryCode = self.txtCountryCode.text ?? ""
        objUser.strMobileNumber = self.txtMobileNumber.text ?? ""
        objUser.strEmailAddress = self.txtEmail.text ?? ""
        objUser.strRegistrationNo = self.txtDoctorRegistration.text ?? ""
        objUser.strBio = self.txtVwDescription.text ?? ""
        objUser.strClinicName = self.txtClinicName.text ?? ""
        objUser.strClinicAddress = self.txtVwLocation.text ?? ""
        
        if dictUserDetail["type"].stringValue == checkDoctorClinic.doctor.rawValue{
            if objUser.isUpdateDoctorProfile() {
                updateProfile()
            } else {
                makeToast(message: objUser.strValidationMessage)
            }
        } else {
            if objUser.isUpdateClinicProfile() {
                updateProfile()
            } else {
                makeToast(message: objUser.strValidationMessage)
            }
        }
        
    }
}

//MARK: - UIImagePickerController

extension EditProfileVC:UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    func alertActionForCamara()  {
        
        // Create the alert controller
        let alertController = UIAlertController(title: APP_NAME, message: "Please choose any one", preferredStyle: .actionSheet)
        
        // Create the actions
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            self.picker.allowsEditing = true
            self.picker.delegate = self
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.picker.modalPresentationStyle = .popover
            self.present(self.picker, animated: true, completion: nil)
            
        }
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.picker.allowsEditing = true
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerController.SourceType.camera
                self.picker.cameraCaptureMode = .photo
                self.picker.modalPresentationStyle = .fullScreen
                self.present(self.picker,animated: true,completion: nil)
            } else {
                makeToast(message: "Sorry, this device has not camera.")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(gallaryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        isSelectedImage = true
        if let editedImage = info[.editedImage] as? UIImage {
            self.imgProfile.image = editedImage
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            self.imgProfile.image = originalImage
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK:- Service

extension EditProfileVC {
    
    func updateProfile(){
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kUpdateProfile)"
            
            print("URL: \(url)")
            
            var param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "user_name":self.txtUserFullName.text ?? "",
                          "email_id":self.txtEmail.text ?? "",
                          "contact_number":self.txtMobileNumber.text ?? "",
                          "bio":self.txtVwDescription.text.trimmingCharacters(in: .whitespaces)
            ]
            
            if dictUserDetail["type"].stringValue == checkDoctorClinic.clinic.rawValue{
                param["clinic_name"] = self.txtClinicName.text ?? ""
                param["address"] = self.txtVwLocation.text
                param["lat"] = "\(clinicLatLong.latitude)"
                param["lng"] = "\(clinicLatLong.longitude)"
            }
            else {
                param["year_experience"] = strStartingYear
            }
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().makeReqeusrWithMultipartData(with: url, method: .post, parameter: param, image: self.imgProfile.image, success: { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.handlerEdirProfile()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }, failure: {(error,code) in
                makeToast(message: error)
            })
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}

