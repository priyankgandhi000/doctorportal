//
//  ForgotPasswordVC.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var txtEmailAddress: UITextField!
    
    
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        [txtEmailAddress].forEach { (txtField) in
            txtField?.delegate = self
        }
    }

}

//MARK:- UITextfield Delegate

extension ForgotPasswordVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}



//MARK:- Action Zone

extension ForgotPasswordVC {
    
    @IBAction func btnSendAction(_ sender:UIButton){
        objUser = User()
        objUser.strEmailAddress = self.txtEmailAddress.text ?? ""
        
        if objUser.isForgotPassword()
        {
            userForgotPassword()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
    }
}

//MARK:- Servie

extension ForgotPasswordVC {
    
    func userForgotPassword()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kForgotPassword)"
            
            print("URL: \(url)")
            
            let param =  ["email_id":objUser.strEmailAddress
                          ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.navigationController?.popViewController(animated: true)
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}
