//
//  LocumChildListVC.swift
//  Doctor
//
//  Created by Jaydeep on 28/08/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class LocumChildListVC: UIViewController {
    
    //MARK:- Vriable Declaratino
    
    var arrClusterLocum:[JSON] = []
    var handlerLocumDetail:(JSON) -> Void = {_ in}
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblLocumList: UITableView!
    
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

       self.tblLocumList.reloadData()
    }
    
    //MARK:- Action ZOne
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    

}

//MARK:- Action Zone

extension LocumChildListVC:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrClusterLocum.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocumCell") as! LocumCell
        let dict = arrClusterLocum[indexPath.row]
        cell.lblHospitalName.text = dict["firm_name"].stringValue
        cell.lblHospitalAddress.text = dict["location"].stringValue
        
        let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
        let strFromDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: startDate)
        let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
        cell.lblFromDate.text = "\(strFromDate) | \(strFromTime)"
        
        let toDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_to"].stringValue)
        let strToDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: toDate)
        let strToTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: toDate)
        
        cell.lblToDate.text = "\(strToDate) | \(strToTime)"
        cell.vwRating.value = CGFloat(dict["avg_rate"].floatValue)
        cell.vwAvgRate.value = CGFloat(dict["avg_rate"].floatValue)
        
        let price = dict["rate_hr"].floatValue * dict["total_hr"].floatValue
        
        let strPrice = numberFormatter().string(from: NSNumber(value: price)) ?? ""
        
        let strFinalCurreny = numberFormatter().string(from: NSNumber(value: dict["rate_hr"].floatValue)) ?? ""
        
        cell.lblRate.text = "Rate: \(dict["currency"].stringValue)\(strFinalCurreny)/hr (\(dict["currency"].stringValue)\(strPrice) for \(dict["total_hr"].stringValue)hr)"
        
        cell.vwStatus.isHidden = true
        switch dict["is_final_status"].stringValue {
        case "2":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Selected"
            } else {
                cell.lblStatus.text = "Hired"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#4CAF50")
            break
        case "3":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Waiting"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#FFC107")
            break
        case "4":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Rejected"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "5":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Completed"
            } else {
                cell.lblStatus.text = "Hired Completed"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#238DFA")
            break
        case "6":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Expired"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "7":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Cancelled"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        default:
            cell.vwStatus.isHidden = true
        }
        
        cell.btnRedirectionOutlet.tag = indexPath.row
        cell.btnRedirectionOutlet.addTarget(self, action: #selector(btnOpenMapsAction), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        handlerLocumDetail(arrClusterLocum[indexPath.row])
        self.dismiss(animated: false, completion: nil)        
    }
    
    @IBAction func btnOpenMapsAction(_ sender:UIButton) {
        let dict = arrClusterLocum[sender.tag]
        let lat = dict["lat"].doubleValue
        let long = dict["lng"].doubleValue
        
        alertActionForOpenMaps(lat: lat, long: long)
    }
}
