//
//  LocumDetailVC.swift
//  Doctor
//
//  Created by Jaydeep on 04/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import HCSStarRatingView
import DropDown
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import SDWebImage

class LocumDetailVC: UIViewController {
    
    var dictLocumDetail = JSON()
    var typeDD = DropDown()
    var arrLocumApplicant:[JSON] = []
    var handlerRefreshLocum:() -> Void = {}
    var isAlreadyApplyOnClinic = false
    var doctorIndex = 0
    var strLocumRequestId = String()
    var arrRate:[JSON] = []
    var dictLocumApplicantDetail = JSON()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblMobileNumber: UILabel!
    @IBOutlet weak var vwRating: HCSStarRatingView!
    @IBOutlet weak var lblHospitalName: UILabel!
    @IBOutlet weak var tblLocumDetail: UITableView!
    @IBOutlet weak var txtLocationName: UITextView!
    //    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var heightOfLocumDetail: NSLayoutConstraint!
    @IBOutlet weak var lblRateHour: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var vwAvgRating: HCSStarRatingView!
    @IBOutlet weak var vwComment: UIView!
    @IBOutlet weak var btnApplyOutlet:UIButton!
    @IBOutlet weak var vwPhoneNumber: UIView!
    @IBOutlet weak var btnCancelOutlet: CustomButton!
    @IBOutlet weak var vwAllRating: UIView!
    @IBOutlet weak var tblRate: UITableView!
    @IBOutlet weak var heightOfTblRate: NSLayoutConstraint!

    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTwoRightMenu(strTitle: "Locum")
        if getUserDetail("dc_user_id") == dictLocumDetail["dc_user_id"].stringValue{
            let btnShareButton = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(btnShareAction))
            btnShareButton.tintColor = UIColor.black
            
            let btnMoreButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_more"), style: .plain, target: self, action: #selector(btnMoreAction))
            btnMoreButton.tintColor = UIColor.black
            
            configureDropdown(dropdown: typeDD, sender: btnMoreButton)
            
            self.navigationItem.rightBarButtonItems = [btnMoreButton,btnShareButton]
            
            
        } else {
            let btnShareButton = UIBarButtonItem(image: #imageLiteral(resourceName: "share"), style: .plain, target: self, action: #selector(btnShareAction))
            btnShareButton.tintColor = UIColor.black
            self.navigationItem.rightBarButtonItems = [btnShareButton]
        }
        tblLocumDetail.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        tblRate.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tblLocumDetail.removeObserver(self, forKeyPath: "contentSize")
        tblRate.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        setupUI()
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblLocumDetail.contentSize.height)")
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
//            doctorIndex = 0
            self.heightOfLocumDetail.constant = tblLocumDetail.contentSize.height
            self.heightOfTblRate.constant  = tblRate.contentSize.height
        }
    }
    
    //MARK:- Setup UI
    
    func setupUI(){
        lblHospitalName.text = dictLocumDetail["firm_name"].stringValue
        
        txtLocationName.text = dictLocumDetail["location"].stringValue
        txtLocationName.sizeToFit()
        txtLocationName.translatesAutoresizingMaskIntoConstraints = true
        txtLocationName.textContainerInset = UIEdgeInsets(top: 5, left: -5, bottom: 5, right: 5)
        
        
        let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dictLocumDetail["date_time_from"].stringValue)
        let strFromDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: startDate)
        let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
        lblStartDate.text = "\(strFromDate) | \(strFromTime)"
        
        let toDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dictLocumDetail["date_time_to"].stringValue)
        let strToDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: toDate)
        let strToTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: toDate)
        
        lblToDate.text = "\(strToDate) | \(strToTime)"
        vwRating.value = CGFloat(dictLocumDetail["rate"].floatValue)
        vwAvgRating.value = CGFloat(dictLocumDetail["avg_rate"].floatValue)
        
        let strFinalCurreny = numberFormatter().string(from: NSNumber(value: dictLocumDetail["rate_hr"].floatValue)) ?? ""
        
        lblRateHour.text = "Rate: \(dictLocumDetail["currency"].stringValue)\(strFinalCurreny)/hr"
        
        let price = dictLocumDetail["rate_hr"].floatValue * dictLocumDetail["total_hr"].floatValue
        
        let strPrice = numberFormatter().string(from: NSNumber(value: price)) ?? ""
        
        lblPrice.text = "(\((dictLocumDetail["currency"].stringValue))\(strPrice) for \(dictLocumDetail["total_hr"].stringValue)hr)"
        
        self.lblComment.text = dictLocumDetail["comments"].stringValue

        self.vwPhoneNumber.isHidden = true
        self.btnApplyOutlet.isHidden = true
        self.btnCancelOutlet.isHidden = true
        self.vwAllRating.isHidden = true
        
        if getUserDetail("dc_user_id") == dictLocumDetail["dc_user_id"].stringValue{
            self.vwPhoneNumber.isHidden = false
            self.lblMobileNumber.text = "\(self.dictLocumDetail["country_code"].stringValue)\(self.dictLocumDetail["contact_number"].stringValue)"
        }
        
        getApplicantDetail()
  
        typeDD.dataSource = ["Edit","Delete"]
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
}

//MARK:- Action Zone

extension LocumDetailVC {
    
    @IBAction func btnOpenMapsAction(_ sender:UIButton) {
        let lat = self.dictLocumDetail["lat"].doubleValue
        let long = self.dictLocumDetail["lng"].doubleValue
        
        alertActionForOpenMaps(lat: lat, long: long)
    }
    
    @objc func btnShareAction() {
        
        let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dictLocumDetail["date_time_from"].stringValue)
        let strFromDate = DateToString(Formatter: dateFormatter.dateFormate7.rawValue, date: startDate)
        let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
        
        let toDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dictLocumDetail["date_time_to"].stringValue)
        
        let strToTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: toDate)
        
        let price = dictLocumDetail["rate_hr"].floatValue * dictLocumDetail["total_hr"].floatValue
        let strComment = dictLocumDetail["comments"].stringValue.isEmpty ? "" : "Bonus/Comment: \(dictLocumDetail["comments"].stringValue)"
        let strPrice = numberFormatter().string(from: NSNumber(value: price)) ?? ""
        
        let text = "Hi, I would like to invite you to use \(APP_NAME). \n\(dictLocumDetail["firm_name"].stringValue) needs a doctor on \(strFromDate) starting from \(strFromTime) to \(strToTime) for rate \(dictLocumDetail["currency"].stringValue)\(dictLocumDetail["rate_hr"].stringValue)/hour (\(dictLocumDetail["currency"].stringValue) \(strPrice) for \(dictLocumDetail["total_hr"].stringValue) hours) at \(dictLocumDetail["location"].stringValue). \n\(strComment) \nDownload the app now from www.doctorportal.info"
        
//        text = "Download the app now from www.doctorportal.info"
        print("text \(text)")
        
        let textShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func btnMoreAction() {
        typeDD.selectionAction = {(index: Int, item: String) in
            switch index {
            case 0:
                print("edit")
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddLocumVC") as! AddLocumVC
                obj.selectedController = .edit
                obj.dictLocumDetail = self.dictLocumDetail
                obj.handlerAddLocum = {[weak self] in
                    self?.handlerRefreshLocum()
                    self?.navigationController?.popViewController(animated: true)
                }
                self.navigationController?.pushViewController(obj, animated: true)
            case 1:
                print("Delete")
                let alertController = UIAlertController(title: APP_NAME, message:"Do you want to delete locum?", preferredStyle: UIAlertController.Style.alert)
                
                let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                    self.deleteLocum()
                }
                let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                    print("Cancel")
                }
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                
            default:
                print("edit")
            }
        }
        typeDD.show()
    }
    
    @IBAction func btnApplyAction(_ sender:UIButton){
        applyOnLocum()
    }
    
    @IBAction func btnCancelLocumAction(_ sender:UIButton){
        let alertController = UIAlertController(title: APP_NAME, message:"Once the locum is cancelled, you can not apply the same locum again. Are you sure you want to cancel it?", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.cancelLocum()
        }
        let cancelAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnMobileNumberAction(_ sender:UIButton){
        let alert = UIAlertController(title: APP_NAME, message: "Please select an option for call", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Phone", style: .default , handler:{ (UIAlertAction)in
            let mobileNumber = "\(self.dictLocumDetail["country_code"].stringValue)\(self.dictLocumDetail["contact_number"].stringValue)"
            guard let number = URL(string: "tel://\(mobileNumber)") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(number)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "WhatsApp", style: .default , handler:{ (UIAlertAction)in
            let mobileNumber = "\(self.dictLocumDetail["country_code"].stringValue)\(self.dictLocumDetail["contact_number"].stringValue)"
            guard let number = URL(string: "whatsapp://send?phone=\(mobileNumber)") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(number, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(number)
            }
          
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel , handler:{ (UIAlertAction)in
            
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    override func btnBackAction(_ sender: UIButton) {
//        handlerRefreshLocum()
        self.navigationController?.popViewController(animated: true)
    }
    
   
}

//MARK:- Tableviewd Delegate and Datasource

extension LocumDetailVC:UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView ==  tblRate {
            return arrRate.count
        }
        return arrLocumApplicant.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblRate {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RateCell") as! RateCell
            let dict = arrRate[indexPath.row]
            cell.lblRatingName.text = dict["title"].stringValue
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocumDetailCell") as! LocumDetailCell
        let dict =  arrLocumApplicant[indexPath.row]
        
        cell.imgProfile.borderWidth = 0.0
        cell.imgProfile.cornerRadius = cell.imgProfile.frame.height / 2
        cell.imgProfile.clipsToBounds = true
        
        cell.btnHireOutlet.tag = indexPath.row
        cell.btnHireOutlet.addTarget(self, action: #selector(btnHiredAction(_:)), for: .touchUpInside)
        cell.btnHireOutlet.isHidden = true
        
        if getUserDetail("dc_user_id") == dictLocumDetail["dc_user_id"].stringValue {
            cell.imgProfile.sd_setImage(with: dict["profile_url"].url , placeholderImage:nil, options: .lowPriority, completed: nil)
            cell.lblDisplayDoctor.isHidden = true
            cell.lblDoctorName.text = dict["user_name"].stringValue
            cell.lblDoctorExperience.text = "\(dict["year_experience"].stringValue) Year experience"
            cell.lblCompleted.text = "\(dict["total_gigs"].stringValue) Completed Locum"
            cell.vwRating.value = CGFloat(dict["rating_average"].floatValue)
            if getUserDetail("dc_user_id") == dictLocumDetail["dc_user_id"].stringValue && dict["is_final_status"].stringValue == "0" {
                cell.btnHireOutlet.isHidden = false
                cell.btnHireOutlet.setTitle("HIRE", for: .normal)
                cell.btnHireOutlet.alpha = 1
                cell.btnHireOutlet.isUserInteractionEnabled = true
            } else if dict["is_final_status"].stringValue == "1" {
                cell.btnHireOutlet.isHidden = false
                cell.btnHireOutlet.setTitle("HIRED", for: .normal)
                cell.btnHireOutlet.alpha = 0.5
                cell.btnHireOutlet.isUserInteractionEnabled = false
            }
        } else {
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue {
                cell.imgProfile.sd_setImage(with: dict["profile_url"].url , placeholderImage:nil, options: .lowPriority, completed: nil)
                cell.lblDisplayDoctor.isHidden = true
                cell.lblDoctorName.text = dict["user_name"].stringValue
                cell.lblDoctorExperience.text = "\(dict["year_experience"].stringValue) Year experience"
                cell.lblCompleted.text = "\(dict["total_gigs"].stringValue) Completed Locum"
                cell.vwRating.value = CGFloat(dict["rating_average"].floatValue)
                if getUserDetail("dc_user_id") == dictLocumDetail["dc_user_id"].stringValue && dict["is_final_status"].stringValue == "0" {
                    cell.btnHireOutlet.isHidden = false
                    cell.btnHireOutlet.setTitle("HIRE", for: .normal)
                    cell.btnHireOutlet.alpha = 1
                    cell.btnHireOutlet.isUserInteractionEnabled = true
                } else if dict["is_final_status"].stringValue == "1" {
                    cell.btnHireOutlet.isHidden = false
                    cell.btnHireOutlet.setTitle("HIRED", for: .normal)
                    cell.btnHireOutlet.alpha = 0.5
                    cell.btnHireOutlet.isUserInteractionEnabled = false
                }
            } else {
                doctorIndex += 1
                cell.lblDisplayDoctor.isHidden = false
                cell.lblDisplayDoctor.text = "Doctor \(doctorIndex)"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblLocumDetail {
            if getUserDetail("dc_user_id") != dictLocumDetail["dc_user_id"].stringValue && getUserDetail("dc_user_id") != arrLocumApplicant[indexPath.row]["dc_user_id"].stringValue {
                return
            }
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "DoctoreDetailVC") as! DoctoreDetailVC
            obj.dictDoctorDetail = arrLocumApplicant[indexPath.row]
            obj.handlerRefreshApplicant = {[weak self] in
                self?.getApplicantDetail()
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}

//MARK:- Action Zone

extension LocumDetailVC {
    
    @objc func btnHiredAction(_ sender:UIButton){
        if arrLocumApplicant[sender.tag]["is_hire"].stringValue == "1" {
            return
        }
        hireLocum(strRequestId: arrLocumApplicant[sender.tag]["dc_locum_request_id"].stringValue)
    }
    
    @IBAction func btnSubmitRateAction(_ sender:UIButton){
        for i in 0..<arrRate.count{
            var dict = arrRate[i]
            if let cell = tblRate.cellForRow(at: IndexPath(row: i, section: 0)) as? RateCell {
                dict["rating"] = JSON(cell.vwRate.value)
            }
            arrRate[i] = dict
        }
        rateToLocum()
    }
}

//MARK:- API

extension LocumDetailVC {
    
    func applyOnLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kApplyLocum)"
            
            print("URL: \(url)")
            
            let param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "dc_locum_id":dictLocumDetail["dc_locum_id"].stringValue]
                          
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
//                        self.getApplicantDetail()
                        self.handlerRefreshLocum()
                        self.btnCancelOutlet.isHidden = true
                        self.btnApplyOutlet.isHidden = true
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func hireLocum(strRequestId:String)
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kLocumHire)"
            
            print("URL: \(url)")
            
            let param =  ["dc_locum_request_id":strRequestId
                ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.getApplicantDetail()
                        self.handlerRefreshLocum()
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    func cancelLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kLocumCancel)"
            
            print("URL: \(url)")
            
            let param =  ["dc_locum_request_id":strLocumRequestId
                          ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["message"].stringValue)
//                        self.getApplicantDetail()
                        self.handlerRefreshLocum()
                        self.btnCancelOutlet.isHidden = true
                        self.btnApplyOutlet.isHidden = true
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func deleteLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kDeleteLocum)"
            
            print("URL: \(url)")
            
            let param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "dc_locum_id":dictLocumDetail["dc_locum_id"].stringValue]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["message"].stringValue)
                        self.handlerRefreshLocum()
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func getApplicantDetail()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kLocumAppicant)"
            
            print("URL: \(url)")
            
            let param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "dc_locum_id":dictLocumDetail["dc_locum_id"].stringValue]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.handlerRefreshLocum()
                        let data = json["data"]
                        self.dictLocumApplicantDetail = data
                        if self.dictLocumDetail["is_cal_status"].stringValue == "5" && data["is_review_enable_for_login_user"].stringValue == "1" {
                            self.arrRate = data["review_list"].arrayValue
                            self.vwAllRating.isHidden = false
                            self.tblRate.reloadData()
                        }
                        self.arrLocumApplicant = data["locum_applicants"].arrayValue
                        
                        if self.arrLocumApplicant.count > 4 {
                            self.btnApplyOutlet.isHidden = true
                            self.btnCancelOutlet.isHidden = true
                        } else {
                            if self.dictLocumDetail["dc_user_id"].stringValue != getUserDetail("dc_user_id") && self.dictLocumDetail["is_final_status"].stringValue == "1" {
                                self.btnCancelOutlet.isHidden = true
                                self.btnApplyOutlet.isHidden = false
                            } else if self.dictLocumDetail["dc_user_id"].stringValue != getUserDetail("dc_user_id") && self.dictLocumDetail["is_final_status"].stringValue == "3" {
                                self.btnCancelOutlet.isHidden = false
                                self.btnApplyOutlet.isHidden = true
//                                self.vwPhoneNumber.isHidden = false
//                                self.lblMobileNumber.text = "\(self.dictLocumDetail["country_code"].stringValue)\(self.dictLocumDetail["contact_number"].stringValue)"
                            }
                            
                            for i in 0..<self.arrLocumApplicant.count{
                                let dict = self.arrLocumApplicant[i]
                                if dict["dc_user_id"].stringValue == getUserDetail("dc_user_id"){
                                    self.strLocumRequestId = dict["dc_locum_request_id"].stringValue
                                }
                                if dict["dc_user_id"].stringValue == getUserDetail("dc_user_id") && dict["is_final_status"].stringValue == "1"{
                                    self.vwPhoneNumber.isHidden = false
                                    self.lblMobileNumber.text = "\(self.dictLocumDetail["country_code"].stringValue)\(self.dictLocumDetail["contact_number"].stringValue)"
                                }
                            }
                            
                            if getUserDetail("type").lowercased() != checkDoctorClinic.doctor.rawValue.lowercased(){
                                self.btnApplyOutlet.isHidden = true
                                self.btnCancelOutlet.isHidden = true
                            }
                        }
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                    self.doctorIndex = 0
                    self.tblLocumDetail.reloadData()
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
    
    func rateToLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kAllRating)"
            
            print("URL: \(url)")
            var arrGiveRate:[JSON] = []
            for i in 0..<arrRate.count {
                var dict = JSON()
                dict["dc_review_list_id"].stringValue = arrRate[i]["dc_review_list_id"].stringValue
                dict["rating"].stringValue = arrRate[i]["rating"].stringValue
                arrGiveRate.append(dict)
            }
            
            let param:[String:Any] =  ["dc_user_id":getUserDetail("dc_user_id"),
                                          "review_type":dictLocumApplicantDetail["review_type"].stringValue,
                                          "global_id":dictLocumApplicantDetail["global_id"].stringValue,
                                          "review_list":arrGiveRate]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.vwAllRating.isHidden = true
                        self.handlerRefreshLocum()
                        self.getApplicantDetail()
                        makeToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["message"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}

