//
//  LocumListVC.swift
//  Doctor
//
//  Created by Jaydeep on 02/07/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class LocumListVC: UIViewController {
    
    //MARK:- Variablde Declaration'
    
    var arrLocumList:[JSON] = []
    var currentLocation = CLLocationCoordinate2D()
    var handlerGetAllLocum:() -> Void = {}
    var strMessage = String()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblLocumList: UITableView!
    
    //MARK:- View Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        currentLocation = kUserCurrentLocation
        self.tblLocumList.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 100, right: 0)
        if dictFilter["lat"].stringValue != "" && dictFilter["lng"].stringValue != ""{
            self.currentLocation = CLLocationCoordinate2D(latitude: dictFilter["lat"].doubleValue, longitude: dictFilter["lng"].doubleValue)
        }
        getAllLocum()
        
        if isComefromPush {
            isComefromPush = false
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumDetailVC") as! LocumDetailVC
            obj.dictLocumDetail = dictPushData
            obj.handlerRefreshLocum = {[weak self] in
                self?.getAllLocum()
            }
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTwoMenu(strTitle: "Locum")
    }
}

//MARK:- Action ZOne

extension LocumListVC {
    
    @IBAction func btnFilterAction(_ sender:UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC") as! FilterVC
        obj.handlerFilterLocum = {[weak self] dict in
            self?.currentLocation = CLLocationCoordinate2D(latitude: dict["lat"].doubleValue, longitude: dict["lng"].doubleValue)
            self?.getAllLocum()
        }
        self.navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func btnCurrentLocation(_ sender:UIButton){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LocumVC.self) {
                handlerGetAllLocum()
                _ =  self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    override func btnAddLocumAction() {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddLocumVC") as! AddLocumVC
        obj.handlerAddLocum = {[weak self] in
            self?.getAllLocum()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}



//MARK:- Tablview Delegate and Datasource

extension LocumListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrLocumList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrLocumList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocumCell") as! LocumCell
        let dict = arrLocumList[indexPath.row]
        cell.lblHospitalName.text = dict["firm_name"].stringValue
        cell.lblHospitalAddress.text = dict["location"].stringValue
        
        let startDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_from"].stringValue)
        let strFromDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: startDate)
        let strFromTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: startDate)
        cell.lblFromDate.text = "\(strFromDate) | \(strFromTime)"
        
        let toDate = StringToDate(Formatter: dateFormatter.dateFormate4.rawValue, strDate: dict["date_time_to"].stringValue)
        let strToDate = DateToString(Formatter: dateFormatter.dateFormate1.rawValue, date: toDate)
        let strToTime = DateToString(Formatter: dateFormatter.dateFormate2.rawValue, date: toDate)
        
        cell.lblToDate.text = "\(strToDate) | \(strToTime)"
        cell.vwRating.value = CGFloat(dict["rate"].floatValue)
        cell.vwAvgRate.value = CGFloat(dict["avg_rate"].floatValue)
        
        let price = dict["rate_hr"].floatValue * dict["total_hr"].floatValue
        
        let strFinalCurreny = numberFormatter().string(from: NSNumber(value: dict["rate_hr"].floatValue)) ?? ""
        
        let strPrice = numberFormatter().string(from: NSNumber(value: price)) ?? ""
        
        cell.lblRate.text = "Rate: \(dict["currency"].stringValue)\(strFinalCurreny)/hr (\(dict["currency"].stringValue)\(strPrice) for \(dict["total_hr"].stringValue)hr)"

        cell.vwStatus.isHidden = true
        cell.lblStatus.text = ""
        
        cell.btnRedirectionOutlet.tag = indexPath.row
        cell.btnRedirectionOutlet.addTarget(self, action: #selector(btnOpenMapsAction), for: .touchUpInside)
        
        switch dict["is_final_status"].stringValue {
        case "2":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Selected"
            } else {
                cell.lblStatus.text = "Hired"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#4CAF50")
            break
        case "3":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Waiting"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#FFC107")
            break
        case "4":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Rejected"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "5":
            cell.vwStatus.isHidden = false
            if getUserDetail("dc_user_id") == dict["dc_user_id"].stringValue{
                cell.lblStatus.text = "Completed"
            } else {
                cell.lblStatus.text = "Hired Completed"
            }
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#238DFA")
            break
        case "6":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Expired"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        case "7":
            cell.vwStatus.isHidden = false
            cell.lblStatus.text = "Cancelled"
            cell.vwStatus.backgroundColor = hexStringToUIColor(hex: "#F44336")
            break
        default:
            cell.vwStatus.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LocumDetailVC") as! LocumDetailVC
        obj.dictLocumDetail = arrLocumList[indexPath.row]
        obj.handlerRefreshLocum = {[weak self] in
            self?.getAllLocum()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnOpenMapsAction(_ sender:UIButton) {
        let dict = arrLocumList[sender.tag]
        let lat = dict["lat"].doubleValue
        let long = dict["lng"].doubleValue
        
        alertActionForOpenMaps(lat: lat, long: long)
    }
}

//MARK:- Service

extension LocumListVC
{
    func getAllLocum()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            self.view.endEditing(true)
            
            let url = "\(kBasicURL)\(kGetLocum)"
            
            print("URL: \(url)")
            
            var param =  ["dc_user_id":getUserDetail("dc_user_id"),
                          "lat":"\(currentLocation.latitude)",
                          "lng":"\(currentLocation.longitude)"]
            
            if dictFilter["start_date"].stringValue != "" && dictFilter["start_time"].stringValue != ""{
                let strStartDate = "\(dictFilter["start_date"].stringValue) \(dictFilter["start_time"].stringValue)"
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: strStartDate)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: finalDate)
                param["date_time_from"]  = strFinalFromDate
            } else  if dictFilter["start_time"].stringValue == "" &&  dictFilter["start_date"].stringValue != ""{
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["start_date"].stringValue)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: finalDate)
                param["date_time_from"] = strFinalFromDate + " 00:00:00"
            } else {
                param["date_time_from"] = ""
            }
            
            if dictFilter["to_date"].stringValue != "" && dictFilter["to_time"].stringValue != ""{
                let strStartDate = "\(dictFilter["to_date"].stringValue) \(dictFilter["to_time"].stringValue)"
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate5.rawValue, strDate: strStartDate)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate4.rawValue, date: finalDate)
                param["date_time_to"]  = strFinalFromDate
            } else  if dictFilter["to_time"].stringValue == "" && dictFilter["to_date"].stringValue != ""{
                let finalDate = StringToDate(Formatter: dateFormatter.dateFormate1.rawValue, strDate: dictFilter["start_date"].stringValue)
                let strFinalFromDate = DateToString(Formatter: dateFormatter.dateFormate6.rawValue, date: finalDate)
                param["date_time_to"] = strFinalFromDate + " 00:00:00"
            } else {
                param["date_time_to"] = ""
            }
            
            param["rate_hr"] = dictFilter["rate_hr"].stringValue
            param["location"]  = dictFilter["location"].stringValue
            
            if currentLocation.latitude == 0.0 && currentLocation.longitude == 0.0{
                param["lat"] = "\(kUserCurrentLocation.latitude)"
                param["lng"] = "\(kUserCurrentLocation.longitude)"
            }
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["res"].stringValue == strSuccessResponse
                    {
                        self.arrLocumList = []
                        self.arrLocumList = json["data"].arrayValue
                    }
                    else
                    {
                        self.arrLocumList = []
                        self.strMessage = json["message"].stringValue
                        if isComefromPush == false {
                            makeToast(message: json["message"].stringValue)
                        }                        
                    }
                     self.tblLocumList.reloadData()
                }
                else
                {
                    makeToast(message: kSomethingWentWrong)
                }
            }
            
        }
        else
        {
            makeToast(message: kNoInternetConnection)
        }
    }
}

